%global pkgname apache-%{project}
%global project maven

%global __os_install_post /usr/lib/rpm/brp-compress %{nil}
%define __requires_exclude_from ^.*\\.jar$
%define __provides_exclude_from ^.*\\.jar$

Name:		maven-dist
Version:	3.3.9
Release:	1%{?dist}
Summary:	Java software project management and comprehension tool
Group:		Development/Tools
License:	ASL 2.0
URL:		https://maven.apache.org
Source0:	http://www-eu.apache.org/dist/maven/maven-3/%{version}/binaries/%{pkgname}-%{version}-bin.tar.gz

BuildRoot: 	%{_tmppath}/%{pkgname}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch

#BuildRequires:	
Requires:	java-devel >= 1:1.7.0

%define mavenhome /usr/share/%{name}

%description
Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.

%prep
%setup -q -n %{pkgname}-%{version}


%build
/bin/true

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{mavenhome}
mv bin boot conf lib $RPM_BUILD_ROOT%{mavenhome}

mkdir -p $RPM_BUILD_ROOT/usr/bin
ln -s %{mavenhome}/bin/mvn $RPM_BUILD_ROOT/usr/bin/mvn

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE NOTICE README.txt
%{mavenhome}*
/usr/bin/mvn

%changelog
* Thu Jan 12 2017 Waldemar Smirnow <waldemar.smirnow@gmail.com>3.3.9-1
- Initial Build
